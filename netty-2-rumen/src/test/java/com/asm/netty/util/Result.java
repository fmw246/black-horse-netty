package com.asm.netty.util;

import com.google.gson.Gson;
import lombok.Data;

import java.io.Serializable;

/**
 * @author fu
 * @create 2022/4/22 14:37
 */
@Data
public class Result implements Serializable {

    private String cmd; //
    private Boolean stn; // /stn: 起始包 true ;应答包：false


    public static String succ(String cmd, Boolean stn) {
        Result r = new Result();
        r.setCmd(cmd);
        r.setStn(stn);


        Gson gson = new Gson();
        return gson.toJson(r);
    }




}
