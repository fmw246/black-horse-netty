package com.asm.AshtonUtil;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringEncoder;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.Charset;
import java.util.Scanner;

@Slf4j
public class 发送JSON字符串1 {

    public static void main(String[] args) throws InterruptedException {

        final NioEventLoopGroup group = new NioEventLoopGroup();

        final Channel channel = new Bootstrap()
                .group(group)
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel ch) throws Exception {
//                        ch.pipeline().addLast(new LoggingHandler(LogLevel.DEBUG));  //----- 日志 ------
                        ch.pipeline().addLast(new StringEncoder());
                        ch.pipeline().addLast(new ChannelInboundHandlerAdapter(){
                            @Override
                            public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
//                                super.channelRead(ctx, msg);
                                ByteBuf buffer = (ByteBuf) msg;
                                log.debug("反馈消息（buffer）：" + buffer.toString());
                                log.debug("反馈消息（解码后 ）：" + buffer.toString(Charset.defaultCharset()));

                            }
                        });

                    }
                })
//                .connect("47.100.173.102", 6000).sync().channel();
//                .connect("localhost", 6611).sync().channel();
                .connect("localhost", 6611).sync().channel();


        new Thread(() -> {
            Scanner scanner = new Scanner(System.in);
            while(true){

                final String line = scanner.nextLine();
                if ("q".equals(line))
                {
                    channel.close();
                    break;
                } else if ("a".equals(line)) {
                    // 如果输入是 a，发送 abc 100 次
                    for (int i = 0; i < 10; i++) {
                        int status = (i % 3) + 1;
                        channel.writeAndFlush("{\"msgType\":\"stateChange\",\"collectorId\":\"0010171331829600\",\"status\":\""+status+"\",\"collectorStatus\":\"2\",\"token\":\"srdazkgoitrczajcakoztocjyofcdjwy\",\"appId\":\"977657829306ba43220f903ea6f6a7de\",\"input\":\"10010000\",\"output\":\"00000011\" }\n");
                    }
                }else{
                    log.debug("发送消息：{}", line);

                }

                channel.writeAndFlush(line);
            }

        }).start();
 /*

         closeFuture() &#x5F02;&#x6B65;&#x7B49;&#x5F85; channel&#x5173;&#x95ED;&#x624D;&#x6267;&#x884C;
         {"a":"1","b":"2","c":"3"}
         ### &#x53EF;&#x91CD;&#x590D;&#x4F7F;&#x7528;&#x7684; &#x767B;&#x5F55;salt ###
         {"msgType":"login","appId":"977657829306ba43220f903ea6f6a7de","salt":"hb12345678","token":"9a2e98dedd149e6e25607e83a4e6fbd7"}
         ### &#x4E0D;&#x53EF;&#x91CD;&#x590D;&#x4F7F;&#x7528; ###
         {"msgType":"login","appId":"977657829306ba43220f903ea6f6a7de","salt":"hb123","token":"68e7287711f78216eeb4740caeb0a42e"}
         ### &#x5176;&#x5B83;appId ###  f0a6ca540cd84e32178e4b82ce311c65
         {"msgType":"login","appId":"3a46a6ed18207f944f24638ea8342e29","salt":"hb12345678","token":"885e910416c3545417fded748c61fbb7"}

{"msgType": "init","appId": "977657829306ba43220f903ea6f6a7de","token": "srdazkgoitrczajcakoztocjyofcdjwy","collectorSet": ["0010171331829600"]}
{"msgType": "init","appId": "977657829306ba43220f903ea6f6a7de","token": "srdazkgoitrczajcakoztocjyofcdjwy","collectorSet": ["0010171331829600","1234567890000002","1234567890000003"]}
{"msgType": "init","appId": "977657829306ba43220f903ea6f6a7de","token": "srdazkgoitrczajcakoztocjyofcdjwy","collectorSet": ["0010171331829600","WNC2410151007054"]}


{"packetCount":"001","msgType":"stateChange","collectorId":"WNC241015","status":"1","collectorStatus":"1","token":"srdazkgoitrczajcakoztocjyofcdjwy","appId":"977657829306ba43220f903ea6f6a7de","input":"10010000","output":"00000011" }

{"msgType":"stateChange","packetCount":"12111","collectorId":"WNC2410151007054","status":"2","collectorStatus":"2","token":"srdazkgoitrczajcakoztocjyofcdjwy","appId":"977657829306ba43220f903ea6f6a7de","input":"10010000","output":"00000011" }



{"msgType":"stateChange","collectorId":"0010171331829600","status":"3","collectorStatus":"3","token":"srdazkgoitrczajcakoztocjyofcdjwy","appId":"977657829306ba43220f903ea6f6a7de","input":"10010000","output":"00000011" }


{"msgType":"niuJu","collectorId":"0010171331829600","niuJu":"336.21","collectorStatus":"1","token":"srdazkgoitrczajcakoztocjyofcdjwy","appId":"977657829306ba43220f903ea6f6a7de"}

{"msgType":"productReporting","collectorId":"0010171331829600","inQuantity":1,"outQuantity":0,"token":"srdazkgoitrczajcakoztocjyofcdjwy","appId":"977657829306ba43220f903ea6f6a7de" }

{"msgType":"wendu","collectorId":"0010171331829600","temperature":"3.33","token":"srdazkgoitrczajcakoztocjyofcdjwy","appId":"977657829306ba43220f903ea6f6a7de" }
{"msgType":"shidu","collectorId":"0010171331829600","humidity":"1","token":"srdazkgoitrczajcakoztocjyofcdjwy","appId":"977657829306ba43220f903ea6f6a7de" }

{"msgType":"windspeed","collectorId":"0010171331829600","windSpeed":"3.33", "token":"srdazkgoitrczajcakoztocjyofcdjwy","appId":"977657829306ba43220f903ea6f6a7de" }

{"msgType":"mpas","collectorId":"0010171331829600",     "mpas":"6.66",      "token":"srdazkgoitrczajcakoztocjyofcdjwy","appId":"977657829306ba43220f903ea6f6a7de" }
{"msgType":"triphase","collectorId":"0010171331829600","dl1":"1.11","dl2":"1.22","dl3":"1.33", "dy1":"2.11","dy2":"2.22","dy3":"2.33", "gl1":"3.11","gl2":"3.22","gl3":"3.33", "token":"srdazkgoitrczajcakoztocjyofcdjwy","appId":"977657829306ba43220f903ea6f6a7de" }

{"msgType":"appWeb","collectorId":"0010171331829600"}



0010171331829600 : 1234
0010170988414703 : 123
        */
        channel.closeFuture().addListener(future -> {
            group.shutdownGracefully();
        });

    }


}
