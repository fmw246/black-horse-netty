package com.asm.AshtonUtil;

import cn.hutool.core.util.StrUtil;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringEncoder;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.Charset;
import java.util.Scanner;

import static ch.qos.logback.core.encoder.ByteArrayUtil.hexStringToByteArray;

@Slf4j
public class 发送16进制字符串 {

    public static void main(String[] args) throws InterruptedException {

        final NioEventLoopGroup group = new NioEventLoopGroup();

        final Channel channel = new Bootstrap()
                .group(group)
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel ch) throws Exception {
                        ch.pipeline().addLast(new StringEncoder());
                        ch.pipeline().addLast(new ChannelInboundHandlerAdapter(){
                            @Override
                            public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
//                                super.channelRead(ctx, msg);
                                ByteBuf buffer = (ByteBuf) msg;
                                log.debug("反馈消息（buffer）：" + buffer.toString());
                                log.debug("反馈消息（解码后 ）：" + buffer.toString(Charset.defaultCharset()));

                            }
                        });

                    }
                })
                .connect("localhost", 6111).sync().channel();


        new Thread(() -> {
            Scanner scanner = new Scanner(System.in);
            while(true){

                final String line = scanner.nextLine();
                if ("q".equals(line))
                {
                    channel.close();
                    break;
                }else{
                    log.debug("发送消息：{}", line);

                }

                byte[] dataToSend = hexStringToByteArray(StrUtil.cleanBlank(line));
                ByteBuf buf = Unpooled.wrappedBuffer(dataToSend);
                channel.writeAndFlush(buf);
            }

        }).start();
        // closeFuture() 异步等待 channel关闭才执行
        // 7e001a0000544c32343031313744315030564237393200008e1d47a4e39e7e
        /**
         7e001a0000544c32343031313744315030564237393200008e1d47a4e39e7e
         7e001a0000303032303230323135323031313635393200008e1d47a4e39e7e
         7e001a0000303032303230323135323031313635303200008e1d47a4e39e7e
         7e001a0000303032303230323135323031313634333200008e1d47a4e39e7e
         7E002C0010303032303230323135323031313633394f94000100126262623236336134313563363835346131357FF47E
         7e001a0000303032303230323135323031313634313200008e3d47a4e39e7e

7e 00 1a 00 00 54 4c 32 34 30 31 31 37 44 31 50 30 56 42 37 39 32 00 00 8e 1d 47 a4 e3 9e 7e 7e 00 1a 00 00 30 30 32 30 32 30 32 31 35 32 30 31 31 36 35 39 32 00 00 8e 1d 47 a4 e3 9e 7e 7e 00 1a 00 00 30 30 32 30 32 30 32 31 35 32 30 31 31 36 35 30 32 00 00 8e 1d 47 a4 e3 9e 7e 7e 00 1a 00 00 30 30 32 30 32 30 32 31 35 32 30 31 31 36 34 33 32 00 00 8e 1d 47 a4 e3 9e 7e 7E 00 2C 00 10 30 30 32 30 32 30 32 31 35 32 30 31 31 36 33 39 4f 94 00 01 00 12 62 62 62 32 36 33 61 34 31 35 63 36 38 35 34 61 31 35 7F F4 7E 7e 00 1a 00 00 30 30 32 30 32 30 32 31 35 32 30 31 31 36 34 31 32 00 00 8e 3d 47 a4 e3 9e 7e
         */
        channel.closeFuture().addListener(future -> {
            group.shutdownGracefully();
        });

    }
}
