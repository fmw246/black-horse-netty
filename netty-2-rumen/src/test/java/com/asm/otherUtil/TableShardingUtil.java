package com.asm.otherUtil;

import java.util.HashMap;
import java.util.Map;

public class TableShardingUtil {
    // 存储表数量与表名前缀的映射
    private static final Map<Integer, String> TABLE_PREFIX_MAP = new HashMap<>();
    static {
        // 初始化表名前缀，可根据实际情况修改
        TABLE_PREFIX_MAP.put(10, "accessory_main_mapping_");
        TABLE_PREFIX_MAP.put(20, "accessory_main_mapping_expanded_");
    }

    /**
     * 根据配件码和表数量计算表名
     * @param accessoryCode 配件码
     * @param tableCount 表的数量
     * @return 对应的表名
     */
    public static String getTableName(String accessoryCode, int tableCount) {
        // 获取表名前缀
        String tablePrefix = TABLE_PREFIX_MAP.get(tableCount);
        if (tablePrefix == null) {
            throw new IllegalArgumentException("Unsupported table count: " + tableCount);
        }
        // 计算哈希值
        int hash = Math.abs(accessoryCode.hashCode());
        // 计算表后缀
        int tableSuffix = hash % tableCount;
        // 生成表名
        return tablePrefix + tableSuffix;
    }

    public static void main(String[] args) {
        String accessoryCode = "your_accessory_code1";
        // 初始表数量为 10
        int initialTableCount = 10;
        String initialTableName = getTableName(accessoryCode, initialTableCount);
        System.out.println("Initial table name: " + initialTableName);

        // 扩展表数量为 20
        int expandedTableCount = 20;
        String expandedTableName = getTableName(accessoryCode, expandedTableCount);
        System.out.println("Expanded table name: " + expandedTableName);
    }
}
