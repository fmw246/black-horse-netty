package com.asm.AshtonUtil;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringEncoder;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.Charset;
import java.util.Scanner;

@Slf4j
public class 发送JSON字符串2 {

    public static void main(String[] args) throws InterruptedException {

        final NioEventLoopGroup group = new NioEventLoopGroup();

        final Channel channel = new Bootstrap()
                .group(group)
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel ch) throws Exception {
//                        ch.pipeline().addLast(new LoggingHandler(LogLevel.DEBUG));  //----- 日志 ------
                        ch.pipeline().addLast(new StringEncoder());
                        ch.pipeline().addLast(new ChannelInboundHandlerAdapter(){
                            @Override
                            public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
//                                super.channelRead(ctx, msg);
                                ByteBuf buffer = (ByteBuf) msg;
                                log.debug("反馈消息（buffer）：" + buffer.toString());
                                log.debug("反馈消息（解码后 ）：" + buffer.toString(Charset.defaultCharset()));

                            }
                        });

                    }
                })
//                .connect("47.100.173.102", 6000).sync().channel();
//                .connect("localhost", 6611).sync().channel();
                .connect("localhost", 6611).sync().channel();


        new Thread(() -> {
            Scanner scanner = new Scanner(System.in);
            while(true){

                final String line = scanner.nextLine();
                if ("q".equals(line))
                {
                    channel.close();
                    break;
                } else if ("a".equals(line)) {
                    // 如果输入是 a，发送 abc 100 次
                    for (int i = 0; i < 10; i++) {
                        int status = (i % 3) + 1;
                        channel.writeAndFlush("{\"msgType\":\"stateChange\",\"collectorId\":\"TL240117TMP0V006\",\"status\":\""+status+"\",\"collectorStatus\":\"2\",\"token\":\"srdazkgoitrczajcakoztocjyofcdjwy\",\"appId\":\"977657829306ba43220f903ea6f6a7de\",\"input\":\"10010000\",\"output\":\"00000011\" }\n");
                    }
                }else{
                    log.debug("发送消息：{}", line);

                }

                channel.writeAndFlush(line);
            }

        }).start();
 /*

         closeFuture() 异步等待 channel关闭才执行
         {"a":"1","b":"2","c":"3"}

{"msgType": "init","appId": "977657829306ba43220f903ea6f6a7de","token": "srdazkgoitrczajcakoztocjyofcdjwy","collectorSet": ["TL240117TMP0V006"]}

{"msgType":"stateChange","collectorId":"TL240117TMP0V006","status":"1","collectorStatus":"1","token":"srdazkgoitrczajcakoztocjyofcdjwy","appId":"977657829306ba43220f903ea6f6a7de","input":"10010000","output":"00000011" }

{"msgType":"niuJu","collectorId":"TL240117TMP0V006","niuJu":"336.21","collectorStatus":"1","token":"srdazkgoitrczajcakoztocjyofcdjwy","appId":"977657829306ba43220f903ea6f6a7de"}

{"msgType": "init","appId": "977657829306ba43220f903ea6f6a7de","token": "srdazkgoitrczajcakoztocjyofcdjwy","collectorSet": ["TL240117TMP0V006"]}


{"msgType":"productReporting","collectorId":"TL240117TMP0V006","inQuantity":1,"outQuantity":0,"token":"srdazkgoitrczajcakoztocjyofcdjwy","appId":"977657829306ba43220f903ea6f6a7de" }

{"msgType":"wendu","collectorId":"TL240117TMP0V006","temperature":"3.33","token":"srdazkgoitrczajcakoztocjyofcdjwy","appId":"977657829306ba43220f903ea6f6a7de" }
{"msgType":"shidu","collectorId":"TL240117TMP0V006","humidity":"1","token":"srdazkgoitrczajcakoztocjyofcdjwy","appId":"977657829306ba43220f903ea6f6a7de" }

{"msgType":"windspeed","collectorId":"TL240117TMP0V006","windSpeed":"3.33", "token":"srdazkgoitrczajcakoztocjyofcdjwy","appId":"977657829306ba43220f903ea6f6a7de" }

{"msgType":"mpas","collectorId":"TL240117TMP0V006",     "mpas":"6.66",      "token":"srdazkgoitrczajcakoztocjyofcdjwy","appId":"977657829306ba43220f903ea6f6a7de" }
{"msgType":"triphase","collectorId":"TL240117TMP0V006","dl1":"1.11","dl2":"1.22","dl3":"1.33", "dy1":"2.11","dy2":"2.22","dy3":"2.33", "gl1":"3.11","gl2":"3.22","gl3":"3.33", "token":"srdazkgoitrczajcakoztocjyofcdjwy","appId":"977657829306ba43220f903ea6f6a7de" }

{"msgType":"appWeb","collectorId":"TL240117TMP0V006"}




TL240117TMP0V006 : 1234
0010170988414703 : 123
        */
        channel.closeFuture().addListener(future -> {
            group.shutdownGracefully();
        });

    }


}
