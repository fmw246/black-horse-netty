package com.asm.AshtonUtil;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringEncoder;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.Charset;
import java.util.Scanner;

@Slf4j
public class 发送JSON字符串3 {

    public static void main(String[] args) throws InterruptedException {

        final NioEventLoopGroup group = new NioEventLoopGroup();

        final Channel channel = new Bootstrap()
                .group(group)
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel ch) throws Exception {
//                        ch.pipeline().addLast(new LoggingHandler(LogLevel.DEBUG));  //----- 日志 ------
                        ch.pipeline().addLast(new StringEncoder());
                        ch.pipeline().addLast(new ChannelInboundHandlerAdapter(){
                            @Override
                            public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
//                                super.channelRead(ctx, msg);
                                ByteBuf buffer = (ByteBuf) msg;
                                log.debug("反馈消息（buffer）：" + buffer.toString());
                                log.debug("反馈消息（解码后 ）：" + buffer.toString(Charset.defaultCharset()));

                            }
                        });

                    }
                })
                .connect("47.100.173.102", 6611).sync().channel();
//                .connect("114.217.18.243", 6611).sync().channel();


        new Thread(() -> {
            Scanner scanner = new Scanner(System.in);
            while(true){

                final String line = scanner.nextLine();
                if ("q".equals(line))
                {
                    channel.close();
                    break;
                }else{
                    log.debug("发送消息：{}", line);

                }

                channel.writeAndFlush(line);
            }

        }).start();
/*  0010171427192300
         closeFuture() 异步等待 channel关闭才执行
         {"a":"1","b":"2","c":"3"}
         ### 可重复使用的 登录salt ###
         {"msgType":"login","appId":"977657829306ba43220f903ea6f6a7de","salt":"hb12345678","token":"9a2e98dedd149e6e25607e83a4e6fbd7"}
         ### 不可重复使用 ###
         {"msgType":"login","appId":"977657829306ba43220f903ea6f6a7de","salt":"hb123","token":"68e7287711f78216eeb4740caeb0a42e"}
         ### 其它appId ###  f0a6ca540cd84e32178e4b82ce311c65
         {"msgType":"login","appId":"3a46a6ed18207f944f24638ea8342e29","salt":"hb12345678","token":"885e910416c3545417fded748c61fbb7"}

{"msgType":"stateChange","collectorId":"0010171331829600","status":"1","collectorStatus":"1","token":"ajjxaorpbmpvwmpgbgxcuojgrzsuqjch","appId":"977657829306ba43220f903ea6f6a7de","input":"10010000","output":"00000011" }
{"msgType":"productReporting","collectorId":"0010171331829600","inQuantity":1,"outQuantity":1,"token":"ajjxaorpbmpvwmpgbgxcuojgrzsuqjch","appId":"977657829306ba43220f903ea6f6a7de" }




{"msgType":"energy","collectorId":"0010171331829600","power":"3.33","voltage":"2.22","current":"1.11","token":"ajjxaorpbmpvwmpgbgxcuojgrzsuqjch","appId":"977657829306ba43220f903ea6f6a7de" }
{"msgType":"climate","collectorId":"0010171331829600","temperature":"3.33","humidity":"1","token":"ajjxaorpbmpvwmpgbgxcuojgrzsuqjch","appId":"977657829306ba43220f903ea6f6a7de" }
{"msgType":"appWeb","collectorId":"0010171331829600"}

{"msgType":"windspeed","collectorId":"0010171331829600","windSpeed":"3.33", "token":"ajjxaorpbmpvwmpgbgxcuojgrzsuqjch","appId":"977657829306ba43220f903ea6f6a7de" }
{"msgType":"mpas","collectorId":"0010171331829600",     "mpas":"6.66",      "token":"ajjxaorpbmpvwmpgbgxcuojgrzsuqjch","appId":"977657829306ba43220f903ea6f6a7de" }
{"msgType":"triphase","collectorId":"0010171331829600","dl1":"1.11","dl2":"1.22","dl3":"1.33", "dy1":"2.11","dy2":"2.22","dy3":"2.33", "gl1":"3.11","gl2":"3.22","gl3":"3.33", "token":"ajjxaorpbmpvwmpgbgxcuojgrzsuqjch","appId":"977657829306ba43220f903ea6f6a7de" }

{"msgType": "init","appId": "977657829306ba43220f903ea6f6a7de","token": "ajjxaorpbmpvwmpgbgxcuojgrzsuqjch","collectorSet": ["0010171331829600"]}

{"msgType": "init","appId": "977657829306ba43220f903ea6f6a7de","token": "ajjxaorpbmpvwmpgbgxcuojgrzsuqjch","collectorSet": ["0010171331829600","1234567890123002","1234567890123003"]}
{"msgType": "init","appId": "977657829306ba43220f903ea6f6a7de","token": "ajjxaorpbmpvwmpgbgxcuojgrzsuqjch","collectorSet": ["0010171331829600","0020202152011663","0010170988414703"]}
{"msgType":"appWeb","collectorId":"0010171331829600"}
{"msgType":"appWeb","collectorId":"1234567890123002"}
{"msgType":"appWeb","collectorId":"0010170988414703"}

*/
        channel.closeFuture().addListener(future -> {
            group.shutdownGracefully();
        });

    }


}
