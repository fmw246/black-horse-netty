package com.asm.netty.g_exercise;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringEncoder;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.Charset;
import java.util.Scanner;

@Slf4j
public class 好用的客户端Client1 {

    public static void main(String[] args) throws InterruptedException {

        final NioEventLoopGroup group = new NioEventLoopGroup();

        final Channel channel = new Bootstrap()
                .group(group)
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel ch) throws Exception {
//                        ch.pipeline().addLast(new LoggingHandler(LogLevel.DEBUG));  //----- 日志 ------
                        ch.pipeline().addLast(new StringEncoder());
                        ch.pipeline().addLast(new ChannelInboundHandlerAdapter(){
                            @Override
                            public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
//                                super.channelRead(ctx, msg);
                                ByteBuf buffer = (ByteBuf) msg;
                                log.debug("反馈消息（buffer）：" + buffer.toString());
                                log.debug("反馈消息（解码后 ）：" + buffer.toString(Charset.defaultCharset()));

                            }
                        });

                    }
                })
//                .connect("47.100.173.102", 6000).sync().channel();
//                .connect("localhost", 6611).sync().channel();
                .connect("localhost", 6611).sync().channel();


        new Thread(() -> {
            Scanner scanner = new Scanner(System.in);
            while(true){

                final String line = scanner.nextLine();
                if ("q".equals(line))
                {
                    channel.close();
                    break;
                }else{
                    log.debug("发送消息：{}", line);

                }

                channel.writeAndFlush(line);
            }

        }).start();
        // closeFuture() 异步等待 channel关闭才执行
        // {"a":"1","b":"2","c":"3"}
        // {"msgType":"register","collectorId":"1234567890123456","modeType":"4"}{"msgType":"re
        // {"msgType":"stateChange","collectorId":"0020202152000001","status":"1","collectorStatus":"5"}
//        {"msgType":"stateChange","collectorId":"0020202152011654","status":"2","collectorStatus":"5"}
//        {"msgType":"productReporting","collectorId":"0020202152011654","processedQuantity":1}
        channel.closeFuture().addListener(future -> {
            group.shutdownGracefully();
        });

    }


}
